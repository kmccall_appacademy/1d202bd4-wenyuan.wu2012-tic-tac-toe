class ComputerPlayer
  attr_accessor :name, :board , :mark

  def initialize(name = 'Bonzo', mark = :O)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    winning_move_available ? winning_move_available : random
  end

  def winning_move_available
    #row
    board.grid.each_with_index do |row_arr, row_idx|
      if row_arr.count(mark) == (row_arr.length - 1) && row_arr.count(nil) == 1
        return [row_idx,row_arr.index(nil)]
      end
    end
    #column
    column_num = board.grid[0].length
     (0..column_num -1).each do |column|
       count_mark = 0
       count_nil = 0
       idx = 0
       (0..board.grid.length-1).each_with_index do |row,row_idx|
         if board.grid[row][column] == nil
           count_nil += 1
           idx = row_idx
         elsif board.grid[row][column] == mark
           count_mark += 1
         end
       end
      comp = board.grid.length - 1
       if count_mark == comp && count_nil == 1
         return [idx,column]
       end
    end
    #diagonal
    #left
    count_mark = 0
    count_nil = 0
    pos = [0,0]
    row_idx = 0
    (0..column_num-1).each do |column|
      if board.grid[row_idx][column] == nil
        count_nil += 1
        pos = [row_idx,column]
      elsif board.grid[row_idx][column] == mark
        count_mark += 1
      end
      row_idx += 1
      end

      if count_mark == (column_num -1) && count_nil == 1
        return pos
      end
    #right
    count_mark = 0
    count_nil = 0
    pos = [0,0]
    row_idx = 0
    (0..column_num-1).to_a.reverse.each do |column|
      if board.grid[row_idx][column] == nil
        count_nil += 1
        pos = [row_idx,column]
      elsif board.grid[row_idx][column] == mark
        count_mark += 1
      end
      row_idx += 1
    end

    if count_mark == (column_num -1) && count_nil == 1
      return pos
    end
  end

  def random
    generate_arr = Array.new(2)
    generate_arr.each_with_index do |el, idx|
      generate_arr[idx] = rand(board.grid.length-1)
    end
    generate_arr
  end

end
