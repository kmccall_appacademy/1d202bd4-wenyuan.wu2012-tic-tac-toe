class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name, mark = :X)
    @name = name
    @mark = mark
  end

  def get_move
    puts "where"
    input = gets.chomp
    arr = input.split(",")
    arr.each_with_index do |el,idx|
      arr[idx] = el.to_i
    end
  end

  def display(board)
    @board = board
    p @board
  end

end
