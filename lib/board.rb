
class Board

  attr_accessor :grid

  def initialize(grid_value = nil)
    @grid = grid_value ? grid_value : [[nil, nil, nil], [nil, nil, nil],[nil, nil, nil]]
  end

  def place_mark(pos,mark)
    self[pos] = mark
  end

 def [](pos)
   row, col = pos
   @grid[row][col]
 end

 def []=(pos, mark)
   row, col = pos
   @grid[row][col] = mark
 end

 def empty?(pos)
   self[pos] == nil ? true : false
 end

def winner
  # row
  self.grid.each do |row_arr|
    first_mark = row_arr[0]
    if row_arr.all? {|el| first_mark == el} && first_mark
      return first_mark
    end
  end
  #column
  column_number = self.grid[0].length

  (0..column_number-1).each do |column|
    result = true
    first_mark = self.grid[0][column]
    self.grid.each do |row_arr|
      row_arr
      if row_arr[column] != first_mark
        result = false
        break
      end
    end
    if result && first_mark
      return first_mark
    end
    end

  #righ_diagonal
  first_mark = self.grid[0][-1]
  result = true
  idx = -1
  self.grid.each do |row_arr|
    if row_arr[idx] != first_mark
      result = false
      break
    end
    idx -= 1
  end

  if result && first_mark
    return first_mark
  end

  #left_diagonal
  first_mark = self.grid[0][0]
  result = true
  idx = 0
  self.grid.each do |row_arr|
    if row_arr[idx] != first_mark
      result = false
      break
    end
    idx += 1
  end

  if result && first_mark
    return first_mark
  end
  # no winner
  nil
end

def over?
  if !self.winner && self.tied?
    true
  else
    self.winner ? true : false
  end
end

def tied?
  result = true
  self.grid.each do |arr_row|
    if arr_row.include?(nil)
      result = false
      break
    end
  end
  result
end

end
