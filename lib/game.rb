require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :arr

  def initialize(player_one,player_two)
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
    @arr = [player_one,player_two]
  end

  def play_turn
    move = current_player.get_move
    board.place_mark(move,:X)
    switch_players!
  end


  def current_player
    arr[0]
  end

  def switch_players!
    arr.reverse!
  end

  def play
    play_turn unless !board.over?
  end

end
